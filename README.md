# jenkins-server

A repo to auto intall a Jenkins server that run on a docker, and execute a python script.

## Requirements

- Docker
- Docker Compose

## Installing the server

In order to install the server:

- Clone the repo using the command:

  `git clone https://gitlab.com/dankadr/jenkins-server.git`
- Change directory to the working directory of the repo:

  `cd jenkins-server`
- run the docker compose to set up the jenkins server:

  `docker-compose up`
