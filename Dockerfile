FROM jenkins/jenkins:2.332.3-lts
USER root
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false
ENV CASC_JENKINS_CONFIG /var/jenkins_home/conf.yaml
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
COPY conf.yaml /var/jenkins_home/conf.yaml
RUN apt-get update && apt-get install -y python3
